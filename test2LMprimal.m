%% Set GLoblal ofset

clear all
%Mesh128_3;  %%%% CHANGE THAT
Meshcr_42_3
T0=T0+1;
NE=3; %%%% CHANGE THAT
NI=NE*(NE-1)/2;
temp=V0==1 | V0==0;
temps2=sum(temp,2);
b=find(temps2==0);
triplot(T0,V0(:,1),V0(:,2),'k');
VT={};
box off
%% Here are the edges:


E = [];
N=max(max(T0));
x=V0(:,1);
y=V0(:,2);
for k=1:size(T0,1)
    for i=1:3
        j = mod(i,3)+1;
        E = [E;T0(k,i) T0(k,j)];
    end
end
B=zeros(N);
E = [min(E(:,1),E(:,2)) max(E(:,1),E(:,2))];
for i=1:size(E,1)
    B(E(i,1),E(i,2))=B(E(i,1),E(i,2))+1;
end
boundaryf=[];
Edgelist=[];
a=[];
for i=1:N
    a=find(B(i,:)==1);
    if(length(a)>0)
        boundaryf=[boundaryf,i,a];
        for j=1:length(a)
            Edgelist=[Edgelist; i a(j)];
        end
    end
    a=[];
end
boudnaryf=unique(boundaryf);
E1=Edgelist(:,1);
E2=Edgelist(:,2);
E1=[E1;Edgelist(:,2)];
E2=[E2;Edgelist(:,1)];
plot(x(boundaryf),y(boundaryf),'ro');
hold on; triplot(T0,x,y)

%%
figure(1)
for i=1:size(T0,1)
     eval(['V=V' num2str(i) ';']);
     VT{i}=V;
end
TF={};
for i=1:size(T0,1)
     eval(['T=T' num2str(i) ';']);
     TF{i}=T+1;
end
interior={};
boundary={};

for i=1:size(T0,1)
    a1=0; a2=0; a3=0;
    b1=0; b2=0; b3=0;
    boundary{i}=[];
 if (ismember(T0(i,1),boundaryf)) boundary{i}=[1]; end
 if (ismember(T0(i,2),boundaryf)) boundary{i}=[ boundary{i},2]; end
 if (ismember(T0(i,3),boundaryf)) boundary{i}=[ boundary{i},3]; end
 if(ismember(T0(i,1),boundaryf) && ismember(T0(i,2),boundaryf))
     a1=(T0(i,1)==E1); b1=(T0(i,2)==E2);
     if(sum(a1&b1))
       boundary{i}=[1,2];
       boundary{i}=[boundary{i},4:4+NE-1];  
     end
 end
 if(ismember(T0(i,2),boundaryf) && ismember(T0(i,3),boundaryf))
     a2=(T0(i,2)==E1); b2=(T0(i,3)==E2);
     if(sum(a2&b2))
     boundary{i}=[boundary{i},[2,3]];
     boundary{i}=[boundary{i},4+NE:4+2*NE-1];
     end
 end
  if(ismember(T0(i,3),boundaryf) && ismember(T0(i,1),boundaryf))
     a3=(T0(i,3)==E1); b3=(T0(i,1)==E2);
     if(sum(a3&b3)) 
     boundary{i}=[boundary{i},[3,1]];
     boundary{i}=[boundary{i},4+2*NE:4+3*NE-1];
     end
  end
  boundary{i}=unique(boundary{i})
  interior{i}=setdiff(1:3+3*NE+((NE-1)*NE)/2,boundary{i});
  plot(VT{i}(interior{i},1),VT{i}(interior{i},2),'ro');
  plot(VT{i}(boundary{i},1),VT{i}(boundary{i},2),'go');
end

% for i=1:size(T0,1);
%     C=VT{i}==0;
%     B=VT{i}==1;
%     CB=sum(B+C,2);
%     interior{i}=find(CB==0);
%     boundary{i}=find(CB~=0);
%     hold on;
%     plot(VT{i}(interior{i},1),VT{i}(interior{i},2),'ro');
%     plot(VT{i}(boundary{i},1),VT{i}(boundary{i},2),'go');
% end
% hold off
%%
T={};
Temp=[];
flag=0;
count=1;
for i=1:size(T0,1)
      a=ismember(TF{i}(:,1),boundary{i});
      b=ismember(TF{i}(:,2),boundary{i});
      c=ismember(TF{i}(:,3),boundary{i});
      d=a+b+c;
      temp=find(~d);
      T{i}=TF{i}(temp,:);
end

%%
V={};
ltgmap={}
for i=1:size(T0,1)
       ltgmap{i}=zeros(max(max(TF{i})),1);
       s=size(interior{i},1);
       ltgmap{i}(interior{i})=1:s;
end


for i=1:size(T0,1)
   V{i}=VT{i}(interior{i},:);
   hold on
   Temp=ltgmap{i}(T{i});
   triplot(Temp,V{i}(:,1),V{i}(:,2));
end
%%
VAll=vertcat(V{:});
VAll=unique(VAll,'rows');
figure(2)
plot(VAll(:,1),VAll(:,2),'ro');

local_to_global={};

for i=1:size(T0,1)
    a=size(V{i},1)-NI;
    temp=[];
    for j=1:a
        for m=1:size(VAll,1)
            if( V{i}(j,1)==VAll(m,1) && V{i}(j,2)==VAll(m,2))
             temp=[temp, m];
            end
        end
    end
    local_to_global{i}=temp;
end
  
verts=local_to_global;

C=horzcat(local_to_global{:});
xinter=unique(C);
mapping=zeros(length(xinter),1);

mapping(xinter)=1:length(xinter);
RG = {};
for i=1:size(T0,1)
 verts{i}=mapping(verts{i});
end

for k=1:size(T0,1)
 	    foo = verts{k};
        RG{k} = sparse(1:length(foo),foo,1,length(foo),length(xinter));
 end

 Rglobal = vertcat(RG{:});
 num = sum(Rglobal,1); 
 W = spdiags(1./num',0,length(num),length(num));
 K1 =Rglobal* W*(Rglobal');

%%

array=[];
for i=0:size(K1,1)-1
	array(i+1)=mod(i,2)+1;
end

F=K1*array';

%%
A={};
Aloc={};
AII={};
AGI={};
AIG={};
AGG={};
interfaceloc={};
triplot(T0,V0(:,1),V0(:,2));
A={};
c={};
sizeint=0;
interfacelocc={};
localiterc={};
for i=1:size(T0,1)
    A{i}=stiff2d(VT{i}',TF{i}');
    plot(VT{i}(interior{i},1),VT{i}(interior{i},2),'ro');
    interfaceloc=setdiff(1:size(VT{i},1)-NI,boundary{i});
    localiter=size(VT{i},1)-NI+1:size(VT{i},1);
    interfacelocc{i}=interfaceloc;
    localiterc{i}=localiter;
    Aloc{i}=A{i}(interior{i},interior{i});
    AII{i}=A{i}(localiter,localiter);
    AGG{i}=A{i}(interfaceloc,interfaceloc);
    AGI{i}=A{i}(interfaceloc,localiter);
    AIG{i}=A{i}(localiter,interfaceloc);
    sizeint=sizeint+size(interfaceloc,2);
    c{i} = LoadVec2D(VT{i}',TF{i}',@Foo);
end

%%
flags=zeros(size(T0,1),1);
for i=1:size(T0,1)
   if(norm(Aloc{i}*ones(size(Aloc{i},1),1))==0)
       flags(i)=1;
   end
end
a=find(flags);
for i=1:size(a,1)
    hold on
   triplot(TF{a(i)},VT{a(i)}(:,1),VT{a(i)}(:,2),'k');
end    
J=zeros(sizeint,size(T0,1));
countt=1;
for i=1:size(T0,1);
      interfaceloc=setdiff(1:size(VT{i},1)-NI,boundary{i}); 
      J(countt:countt+length(interfaceloc)-1,i)=ones(length(interfaceloc),1)./sqrt(length(interfaceloc));
      countt=countt+length(interfaceloc);
end
b=setdiff(1:size(T0,1),a);
sizeJ=size(J,1);
for i=1:size(b,2);
    J(:,b(i))=zeros(size(J,1),1);
end    

%%
S={};
Q={};
a=0.73;%%%%%%%%%%%%%% a
g={};
fI={};
fG={};
for i=1:size(T0,1)
    fI{i}=c{i}(localiterc{i});
    fG{i}=c{i}(interfacelocc{i});
    sizeint=sizeint+size(interfaceloc,2);
    c{i} = LoadVec2D(VT{i}',TF{i}',@Foo);
    S{i}=AGG{i}-AGI{i}*(AII{i}\AIG{i});
    Q{i}=a*inv(S{i}+a*eye(size(S{i})));
    g{i}=fG{i}-AGI{i}*(AII{i}\fI{i});
end

%lambda=(Q-K)\(-Q*g); % We finde the Langrage multipliers.
%%
%%Mo=blkdiag(MM{:});
S1=S;
S=blkdiag(S{:});
I=eye(size(S));
MMO=zeros(size(S));
K=K1;
MK=K;
size(K)
size(S)
b=1;
a1=max(eig(full(S)));
b1=min(eig(full(S)));
a=(a1+b1)/2;
g=vertcat(g{:});
Q={};

Q=blkdiag(Q{:});
%%%%%%%%%%%%%% a
 %C=blkdiag((S+a*I), I/a,-(K+I));
 
 B=[(S+a*I) , K , -(K+I)  ;
     K,     I/a,   MMO;
    (K+I)*S MMO  -(K+I)];

C=[g-K*g;zeros(length(g),1);g];
%uG=B\C;
%BB=[-K*S+a*I, K; K I/a];
%CC=-(I-K)*K*S+a*I-a*K;
% CC1=(-K*(S+a*I)+a*I);
%%
y=randn;
z=randn;
a=1;
b1=0.0279;
y=(a1)/2;
a=y/4;

%%
CC2=(full((2*y*I-K*S)*(K*S+a*I-a*K+1/a1*K*S)));
CC3=(K*S+a*I-a*K);
CC1=((2*y*I-K*S)*(K*S-a*I+a*K));
% CC3=K*S-a*K+a/b*I;
% cond(CC3)
spy(CC3)
DD=diag(diag(S));
Sinv=inv(DD)
spy(CC2)
%P=inv(2*a1*-a1*J*J'*K*J*J');spy(CC2)
%plot(real(eig(full(P*CC2))),'o')
%cond(P*CC2)
%cond(CC1)
%a1/b1
%plot(real(eig(full(CC2))),'o')
%cond(P*CC2)
full(CC2)
cond(CC2)
cond(CC3)
inv(I-J*J'*K*J*J')
spy(inv(I-J*J'*K*J*J'))
%%
eig(CC2);
uG=CC3\(K*g);
%%
%uG=CC\(-K*g);
%uG=B\G;
%uG=B\[g-K*g;zeros(length(g),1);g];
%[uG,flag,relres,iter] = gmres(CC3,[K*g],size(CC2,1),1e-14,size(CC2,1))

%%
%lambda=uG(length(g)+1:2*length(g))
%lambda2=uG(2*length(g)+1:end);
x1={};
x2={};
x3={};
%x1=interloc; 
x3=localiterc; 
x2=interfacelocc;
k=0;
uk={};

useb = {};
for i=1:size(T0,1)
    uk{i}=AII{i}\(fI{i}-AIG{i}*(uG(k+1:k+size(x2{i},2)))) ;
    u0 = zeros(length(x2{i})+length(x3{i}),1);
    u0(x2{i},1) = uG(k+1:k+size(x2{i},2));
    u0(x3{i},1) = uk{i}(1:length(x3{i}),1);
     k=k+size(x2{i},2);
     x0=[x2{i},x3{i}];
     x=VT{i}(:,1);
     y=VT{i}(:,2);
    patch(x(T{i}'),y(T{i}'),u0(T{i}'),u0(T{i}'));
    useb{i} = u0;
end

