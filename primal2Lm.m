%primal 2Lm (maybe)%

clear all;
node = [0,0; 2,0; 2,2; 0,2];
elem = [2,3,1; 4,1,3];

for k = 1:4
    [node,elem] = uniformbisect(node,elem);
end
x=node(:,1);
y=node(:,2);
T=elem;

showmesh(node,elem);
%%
m=2;
numbef_of_subdomains=2^(2*m);
n=sqrt(numbef_of_subdomains);
if(flag==1)
fprintf('Use penalty method');
end
if(flag==0)
    fprintf('Statndar finite elements')
end
flag=0;
ax=max(x); ay=max(y);
bx=min(x); by=min(y);

xdis=abs(ax-bx);
ydis=abs(ay-by);

subdomains=zeros(numbef_of_subdomains,4);

sdx=unique([bx:xdis/(n):ax,ax]);
sdy=unique([by:ydis/(n):ax,ay]);
count=0;
sdtemp=zeros(n,2);
sdtemp(1,1)=sdx(1);
sdtemp(1,2)=sdx(2);
for i=2:length(sdx)-1
    sdtemp(i,1)=sdtemp(i-1,2);
    sdtemp(i,2)=sdx(i+1);
end
sdtempy=zeros(n,2);
sdtempy(1,1)=sdy(1);
sdtempy(1,2)=sdy(2);
for i=2:length(sdy)-1
    sdtempy(i,1)=sdtempy(i-1,2);
    sdtempy(i,2)=sdy(i+1);
end


%%
sdtempy=repmat(sdtempy,n,1);
sdtempx=zeros(size(sdtempy));
count=0;
for i=1:size(sdtemp,1)
    sdtempx(count+1:count+n,1:end)=repmat(sdtemp(i,:),n,1);
    count=count+n;
end
%%
size(sdtempy);
subdomains(1:end,1:2)=sdtempx;
subdomains(1:end,3:4)=sdtempy;
%%
sd=subdomains;
verts={};
for j=1:size(sd,1)
    verts{j}= find((x>=sd(j,1)) & (x<=sd(j,2)) & (y>=sd(j,3)) & (y<=sd(j,4)));
    scatter(x(verts{j}),y(verts{j}),40,rand(1,3),'s','filled')
    hold on;
end
%% find globalboundary
globalboundary=findboundary(elem);
plot(x(globalboundary),y(globalboundary),'o');
%% find interface points %%
interface=zeros(length(x),1);
for j=1:size(sd,1)
    interface(verts{j})=interface(verts{j})+1;   
end
interfaceind=find(interface>1);
temp=setdiff(interfaceind,globalboundary);
interfaceind=temp;
interface=zeros(length(x),1);
interface(temp)=1;
interface(globalboundary)=2;
plot(x(temp),y(temp),'o')
bound_inter=find(interface>0);
plot(x(bound_inter),y(bound_inter),'ro')
%% interior;
interiorD={};
for i=1:size(sd,1)
    interiorD{i}=setdiff(verts{i},bound_inter);
    plot(x(interiorD{i}),y(interiorD{i}),'ro');
    hold on;
end
%% Local to global mapping
global_to_local={};

for i=1:size(sd,1)
    temp=zeros(length(x),1);
    temp(verts{i})=[1:length(verts{i})];
    global_to_local{i}=temp;
end

%%  Find triangles
triangles={};
for j=1:size(sd,1)
    %v0=[];
    %v0(verts{j}) = 1:length(verts{j}); 
    xloc{j} = x(verts{j}); % x in subdomain j
    yloc{j} = y(verts{j}); % y in subdomain j
    %inside is a boolean vector
    inside = ((x>=sd(j,1)) & (x<=sd(j,2)) & (y>=sd(j,3)) & (y<=sd(j,4)));
    %triangles finds which triangles belong to the subdomain
    triangles{j} = find(inside(T(:,1)) & inside(T(:,2)) & inside(T(:,3)));
%    Tloc{j} = v0(T(triangles{i},:));
%    x0{j} = xloc{j};
%    y0{j} = yloc{j};
%    T0{j} = Tloc{j};
%    patch(x0{j}(T0{j}'),y0{j}(T0{j}'),j.*ones(size(T0{j}')));
    
end



%%
A={};
Aloc={};
AII={};
AGI={};
AIG={};
AGG={};
fI={};
fG={};
for i=1:size(sd,1)
    x0=x(verts{i}); y0=y(verts{i});
    nodeloc=[x0,y0];
    elemloc=global_to_local{i}((elem(triangles{i},:)));
     A=stiff2d(nodeloc',elemloc');
    f=LoadVec2D(nodeloc',elemloc',@Foo);
    [tempG]=find(interface(verts{i})==1);
    [tempI]=find(interface(verts{i})==0);
    if (flag==1)
      local_boundary=find(interface(verts{i})==2);
      tempI=[find(interface(verts{i})==0);find(interface(verts{i})==2)];
       A(local_boundary,local_boundary)=10^6;     
    end
    interfaceloc{i}=tempG;
    interiorloc{i}=tempI;
    fI{i}=f(tempI);
    fG{i}=f(tempG);
    AII{i}=A(tempI,tempI);
    AGG{i}=A(tempG,tempG);
    AGI{i}=A(tempG,tempI);
    AIG{i}=A(tempI,tempG);
end

%%
S={};
Q={};
a=0.73;%%%%%%%%%%%%%% a
g={};

sizeint=0;
for i=1:size(sd,1);
  
    sizeint=sizeint+size(interfaceloc{i},1);
    S{i}=AGG{i}-AGI{i}*(AII{i}\AIG{i});
    Q{i}=a*inv(S{i}+a*eye(size(S{i})));
    g{i}=fG{i}-AGI{i}*(AII{i}\fI{i});
end

%%
RG = {};
testfoo={};

for k=1:size(sd,1)
    foo = intersect(verts{k},interfaceind);
    RG{k} = sparse(1:length(foo),foo,1,length(foo),length(x));
end

Rglobal = vertcat(RG{:});
%spy(Rglobal)
num = sum(Rglobal,1); 
W = spdiags(1./num',0,length(num),length(num));
K = Rglobal*W*(Rglobal');

%%
Q=blkdiag(Q{:});
G=[];
G=vertcat(g{:});


QMINK2=(Q-K);
IMIQK=(eye(size(K))-2*K)*QMINK2;

sol1=(QMINK2)\(-Q*G);
%% We recover the solution of the problem for each subdomain.
count=0;
for i=1:size(sd,1)
    y1=[];x1=[];
    n=size(interfaceloc{i},1);
    m=size(interiorloc{i},1);
    u=[AII{i} AIG{i}; AGI{i} (AGG{i}+a*eye(size(AGG{i})))]\([fI{i};fG{i}]...
                                +[zeros(m,1);sol1(count+1:count+n)]);
    count=count+n;
    u0 = zeros(length(verts{i}),1);
    u0(interfaceloc{i},1) = u(end-n+1:end,1);
    u0(interiorloc{i},1) = u(1:m,1);
    x1=x(verts{i}); y1=y(verts{i});
    nodeloc=[x1,y1];
    elemloc=global_to_local{i}((elem(triangles{i},:)));
    
    patch(x1(elemloc'),y1(elemloc'),u0(elemloc'),...
        randn*ones(size(u0(elemloc'))));
   
    hold on
end










